//! Defines an `Operation` as a (String, u64) pair. This is the building block of a `Program`.
//! Defines the `Program` trait and provides two stucts that implement it: `BasicProgram` and
//! `InstrumentProgram`. Both structs also implement `Deserialize` for ease of use with JSON inputs.

use serde::Deserialize;

// TODO: Stop using `pub` attributes, add From impls, new() functions, other methods for
// creating/operating on the structures.

/// A possible operation, composed of a `String` (the operator) and a `u64` (the operand)
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq)]
pub struct Operation {
    #[serde(rename = "type")]
    operation_type: String,
    value: u64, // TODO: replace this with something more general. Option<u64>? Json Value?
}

/// Defines a `Program`, i.e. anything that has an initial value and a series of `Operation`s
pub trait Program: Send + Sync {
    fn initial_value(&self) -> u64;
    fn operations(&self) -> &Vec<Operation>;
}

/// A basic `Program`, no associated instrument
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq)]
pub struct BasicProgram {
    pub initial_value: u64,
    pub operations: Vec<Operation>,
}

/// A `Program` and a `String` which defines the name of the instrument the program is meant to run
/// on
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq)]
pub struct InstrumentProgram {
    pub control_instrument: String,
    pub initial_value: u64,
    pub operations: Vec<Operation>,
}

impl Operation {
    pub fn operation_type(&self) -> &str {
        &self.operation_type
    }
    pub fn value(&self) -> u64 {
        self.value
    }
}

impl Program for InstrumentProgram {
    fn initial_value(&self) -> u64 {
        self.initial_value
    }
    fn operations(&self) -> &Vec<Operation> {
        &self.operations
    }
}

impl Program for BasicProgram {
    fn initial_value(&self) -> u64 {
        self.initial_value
    }
    fn operations(&self) -> &Vec<Operation> {
        &self.operations
    }
}

impl From<u64> for BasicProgram {
    #[inline]
    fn from(initial_value: u64) -> Self {
        BasicProgram { initial_value: initial_value, operations: Vec::new() }
    }
}

impl From<(String, u64)> for Operation {
    #[inline]
    fn from((operation_type, value): (String, u64)) -> Self {
        Operation { operation_type: operation_type, value: value }
    }
}
