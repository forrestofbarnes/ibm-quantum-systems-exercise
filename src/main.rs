//! This program is a simulacrum of a program that could be used to control quantum computers. It
//! takes a series of arithmetic operations (defined in a JSON file) as input, transforms them into
//! a set of "pulses" (as one might send pulses to quantum hardware), sends the pulses to a set of
//! designated remote services, and prints the results calculated by those services.

use std::collections::HashMap;
use std::sync::Arc;

use futures::future;

pub mod error;
pub mod instrument;
pub mod program;

pub use error::Error;
pub use instrument::Instrument;
use program::Program;
pub use program::{InstrumentProgram, Operation};

/// Maps `&str` instrument names to `Instrument`s.
// TODO: &'static str seems appropriate for the job, but is inflexible. Consider &'a or String.
pub type InstrumentMap = HashMap<&'static str, Arc<dyn Instrument>>;

/// Turns an `InstrumentProgram` into a program running on a particular instrument. Returns a
/// `Future` of the output.
pub async fn run_instrument_program
    (program: &InstrumentProgram, instrument_map: &InstrumentMap) -> Result<u64, Error>
{
    // Would do the lowercasing in Serde, but couldn't figure out how to do it simply
    let instrument_name = program.control_instrument.to_lowercase();
    let instr = match instrument_map.get(instrument_name.as_str()) {
        Some(value) => value.clone(),
        None => Err(error::UnknownInstrument(instrument_name))?
    };
    Ok(instr.run(program).await?)
}

/// Turns an iterator over `&InstrumentProgram`s into a joined `Future` of all outputs. This ensures
/// the `Future`s run in parallel.
pub async fn run_all_instrument_programs<'a, I>
    (programs: I, instrument_map: &InstrumentMap) -> Result<Vec<u64>, Error>
where
    I: Iterator<Item = &'a InstrumentProgram>
{
    future::try_join_all(programs.map(|p| run_instrument_program(&p, instrument_map))).await
}

/* This code was designed like a library with binary extras. All the "extras" which would be
 * unnecessary in a library are collected below this comment.
 */
use std::ffi::OsString;

use clap::{Parser, ValueHint};

use instrument::{Local, Acme, Madrid};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = "Runs programs specified by the input JSON")]
struct Args {
    /// Quantum program input file
    #[clap(index = 1, value_parser, value_hint = ValueHint::FilePath)]
    json: OsString,
    /// Url of Acme Instrument Service
    #[clap(short, long, value_parser, value_name = "URL", value_hint = ValueHint::Url)]
    acme: Option<String>,
    /// Url of Madrid Instrument Service
    #[clap(short, long, value_parser, value_name = "URL", value_hint = ValueHint::Url)]
    madrid: Option<String>,
    /// Test against local service instead of sending outputs to stdout
    #[clap(long, action)]
    test: bool,
    // Couldn't figure out how to use #[test] when the server addresses will likely change. This
    // --test argument is a compromise.
}

/* One of the coolest things about Rust is that with one change to the following line of code, the
 * program can go from multi-threaded parallelism to single-threaded parallelism. The performance
 * probably wouldn't even change much because most of the program's runtime is spent waiting for the
 * server.
 */
#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Args::parse();

    let mut instrument_map: InstrumentMap = HashMap::new();
    let local = Arc::from(Local);
    instrument_map.insert("local", local.clone());
    if let Some(url) = args.acme {
        instrument_map.insert("acme", Arc::from(Acme::from(url)));
    }
    if let Some(url) = args.madrid {
        instrument_map.insert("madrid", Arc::from(Madrid::from(url)));
    }

    let mut programs: Vec<InstrumentProgram> = Vec::new();

    let json_file = std::fs::read(args.json)?;

    // Try to serialize to a Vec<InstrumentProgram>, and if that fails, a normal InstrumentProgram.
    // Serde is absolute MAGIC.
    match serde_json::from_slice(json_file.as_slice()) {
        Ok(program_vec) => {
            programs = program_vec;
        },
        Err(_) => {
            programs.push(serde_json::from_slice(json_file.as_slice())?);
        }
    }

    // Run *ALL* the programs! In parallel!
    let outputs = run_all_instrument_programs(programs.iter(), &instrument_map).await?;

    // Handle outputs
    if args.test {
        for (i, (prog, out)) in programs.iter().zip(outputs).enumerate() {
            assert_eq!(local.run(prog).await?, out, "{i}");
        }
    }
    else {
        for out in outputs {
            println!("{out}");
        }
    }
    Ok(())
}
