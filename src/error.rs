//! Errors used by the program

use std::{fmt, io};

/// Error for when an InstrumentProgram specifies an unknown `Instrument`
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct UnknownInstrument(pub String);

/// Error for when an `Operation` has a type or value not supported by the current `Instrument`
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct UnsupportedOperation(pub String);

/// Unified error type for all possible errors within the library
#[derive(Debug)]
pub enum Error {
    IOError(io::Error),
    Parse(serde_json::Error),
    Reqwest(reqwest::Error),
    Operation(UnsupportedOperation),
    Instrument(UnknownInstrument),
}

impl std::error::Error for UnknownInstrument {}
impl fmt::Display for UnknownInstrument {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unknown instrument: {:?}", self.0)
    }
}

impl std::error::Error for UnsupportedOperation {}
impl fmt::Display for UnsupportedOperation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "unsupported operation: {:?}", self.0)
    }
}

impl std::error::Error for Error {}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IOError(err) => err.fmt(f),
            Self::Parse(err) => err.fmt(f),
            Self::Reqwest(err) => err.fmt(f),
            Self::Operation(err) => err.fmt(f),
            Self::Instrument(err) => err.fmt(f),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::IOError(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Self {
        Error::Parse(err)
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        Error::Reqwest(err)
    }
}

impl From<UnsupportedOperation> for Error {
    fn from(err: UnsupportedOperation) -> Self {
        Error::Operation(err)
    }
}

impl From<UnknownInstrument> for Error {
    fn from(err: UnknownInstrument) -> Self {
        Error::Instrument(err)
    }
}
