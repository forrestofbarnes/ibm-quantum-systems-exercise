//! Module of `Instrument`s

use async_trait::async_trait;

use crate::Program;
use crate::error::Error;

mod local;
mod acme;
mod madrid;

pub use local::Local;
pub use acme::Acme;
pub use madrid::Madrid;

/// Defines an `Instrument`, i.e. anything that can run a `Program`
#[async_trait]
pub trait Instrument {
    async fn run<'a>(&self, program: &'a dyn Program) -> Result<u64, Error>;
}
