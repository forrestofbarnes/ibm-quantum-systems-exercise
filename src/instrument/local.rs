//! The `Local` instrument

use async_trait::async_trait;

use crate::error::{Error, UnsupportedOperation};
use crate::instrument::Instrument;
use crate::Program;

/// An `Instrument` that runs locally instead of on a remote
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct Local;

#[async_trait]
impl Instrument for Local {
    async fn run<'a>(&self, program: &'a dyn Program) -> Result<u64, Error> {
        let mut out = program.initial_value() as f64;
        for op in program.operations() {
            let value = op.value() as f64;
            match op.operation_type() {
                "Add" => out += value,
                "Sum" => out += value,
                "Diff" => out -= value,
                "Sub" => out -= value,
                "Mul" => out *= value,
                "Div" => out /= value,
                op_type => Err(UnsupportedOperation(op_type.to_string()))?
            }
        }
        Ok(out as u64)
    }
}

#[cfg(test)]
mod test {
    use crate::program::BasicProgram;
    use super::*;
    #[tokio::test]
    async fn basic() {
        let mut program = BasicProgram::from(3);
        program.operations.push(("Add".to_string(), 2).into());
        assert_eq!(Local.run(&program).await.unwrap(), 5);
        program.operations.push(("Sum".to_string(), 2).into());
        assert_eq!(Local.run(&program).await.unwrap(), 7);
        program.operations.push(("Diff".to_string(), 3).into());
        assert_eq!(Local.run(&program).await.unwrap(), 4);
        program.operations.push(("Sub".to_string(), 1).into());
        assert_eq!(Local.run(&program).await.unwrap(), 3);
        program.operations.push(("Mul".to_string(), 5).into());
        assert_eq!(Local.run(&program).await.unwrap(), 15);
        program.operations.push(("Div".to_string(), 3).into());
        assert_eq!(Local.run(&program).await.unwrap(), 5);
    }
    #[tokio::test]
    async fn hidden_float() {
        let mut program = BasicProgram::from(1);
        program.operations.push(("Div".to_string(), 4).into());
        assert_eq!(Local.run(&program).await.unwrap(), 0); // Underlying value = 1/4
        program.operations.push(("Add".to_string(), 1).into());
        assert_eq!(Local.run(&program).await.unwrap(), 1); // Underlying value = 5/4
        program.operations.push(("Mul".to_string(), 4).into());
        assert_eq!(Local.run(&program).await.unwrap(), 5);
    }
}