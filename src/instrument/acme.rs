//! The `Acme` instrument

use async_trait::async_trait;
use reqwest::Client;
use serde::Deserialize;
use serde_json::{Value, Map};

use crate::error::{Error, UnsupportedOperation};
use crate::instrument::Instrument;
use crate::Program;

// Helper structs for deserializing response JSON
#[derive(Deserialize)]
struct InitialResponse {
    program_id: String
}

#[derive(Deserialize)]
struct ProgramResult {
    result: u64
}

/// Maintains a connection to the ACME instrument
#[derive(Clone, Debug)]
pub struct Acme {
    url: String,
    client: Client, // TODO: use just one Client for all external connections? Maybe not, messy.
}

impl From<String> for Acme {
    #[inline]
    fn from(url: String) -> Self {
        Acme {
            url: url,
            client: Client::new(),
        }
    }
}

#[async_trait]
impl Instrument for Acme {
    async fn run<'a>(&self, program: &'a dyn Program) -> Result<u64, Error> {
        let mut program_code = Vec::with_capacity(program.operations().len()*3 + 2); // Minimum size
        program_code.extend([
            Value::String("Acme_initial_state_pulse".to_string()),
            Value::Number(program.initial_value().into()),
        ]);
        for op in program.operations() {
            match op.operation_type() {
                "Add" => program_code.extend([
                    Value::String("Acme_pulse_1".to_string()),
                    Value::String("Acme_pulse_2".to_string()),
                    Value::Number(op.value().into()),
                ]),
                "Sum" => program_code.extend([
                    Value::String("Acme_pulse_1".to_string()),
                    Value::String("Acme_pulse_2".to_string()),
                    Value::Number(op.value().into()),
                ]),
                "Mul" => program_code.extend([
                    Value::String("Acme_pulse_2".to_string()),
                    Value::String("Acme_pulse_1".to_string()),
                    Value::String("Acme_pulse_1".to_string()),
                    Value::Number(op.value().into()),
                ]),
                "Div" => program_code.extend([
                    Value::String("Acme_pulse_2".to_string()),
                    Value::String("Acme_pulse_2".to_string()),
                    Value::Number(op.value().into()),
                ]),
                op_type => Err(UnsupportedOperation(op_type.to_string()))?,
            }
        }
        let request = Value::Object(Map::from_iter([
            ("program_code".to_string(), Value::Array(program_code))
        ]));

        let post_url = [self.url.clone(), "load_program".to_string()].join("/");
        let post_response = self.client.post(post_url).json(&request).send().await?;
        // TODO: Improve error message in case of invalid response
        let program_id = post_response.json::<InitialResponse>().await?.program_id;

        let get_url = [self.url.clone(), "run_program".to_string(), program_id].join("/");
        let get_response = self.client.get(get_url).send().await?;
        // TODO: Improve error message in case of invalid response
        Ok(get_response.json::<ProgramResult>().await?.result)
    }
}
