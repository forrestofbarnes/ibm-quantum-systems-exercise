
# IBM Quantum Systems software exercise

This is the submission of the Quantum Systems software exercise by Forrest Barnes, for his
application to the Quantum Backend Software position.

## Purpose

This program is a simulacrum of a program that could be used to control quantum computers. It takes
a series of arithmetic operations (defined in a JSON file) as input, transforms them into a set of
"pulses" (as one might send pulses to quantum hardware), sends the pulses to a set of designated
remote services, and prints the results calculated by those services.

### Sample input file
```json
{
  "id": "abcdefghijkl",
  "control_instrument": "ACME",
  "initial_value": 10,
  "operations": [
    {
      "type": "Sum",
      "value": 120
    },
    {
      "type": "Mul",
      "value": 3
    },
    {
      "type": "Div",
      "value": 2
    }
  ]
}
```

This instructs the `ACME` service to do the following calculation: `((10 + 120)*3)/2`. The expected
result is `195`. One can also run multiple programs at once by making the input file a list of
programs:

```json
[
  {
    "id": "Over Nine Thousand",
    "control_instrument": "acme",
    "initial_value": 9000,
    "operations": [
      {
        "type": "Sum",
        "value": 1
      }
    ]
  },
  {
    "id": "Funny Number",
    "control_instrument": "madrid",
    "initial_value": 69000,
    "operations": [
      {
        "type": "Sum",
        "value": 420
      }
    ]
  }
]
```

### Services

This program supports the `ACME` and `Madrid` services (the `control_instrument` field is not
case-sensitive), and communicates with them by HTTP request. To use the `ACME` or `Madrid` services,
provide the URLs of the required service(s) to the `--acme` and `--madrid` options.

This program also supports a `Local` service that runs in-program and does not require a URL.

## Installation

1. Install the latest version of Rust from https://www.rust-lang.org/tools/install
1. Run `cargo build` in the root directory

## Arguments and options

`cargo run -- --help` produces a good summary of the arguments and options used by this library.

The first (and only) positional argument is the `.json` input file.

The `--acme` and `--madrid` options each take a string input: the URL of the associated service.

The `--test` option is used for internal testing to ensure each service produces the expected
results.

### Example

```
cargo run -- large_quantum_program.json --acme http://127.0.0.1:8000 --madrid http://localhost:8001 --test
```
